﻿using System;
using System.Threading;
using System.Drawing;
using System.Drawing.Imaging;
using System.Diagnostics;

namespace grabstre
{
    internal class ScreenPoller
    {
        public void Start() {
            var t = new Thread(ScreenPolling);
            t.Name = "ScreenPolling";
            t.IsBackground = true;
            t.Start();

            /*var processingT = new Thread(ProcessingData);
            processingT.Name = "ProcessingData";
            processingT.Start();*/
        }

        public long iterationsRan = 0;
        public long iterationsLastSecond = 0;
        

        int width = 100;
        int height = 100;
        private Rectangle rekt;

        public Bitmap capturingIntoBitMap;
        private Graphics graphicsThing;

        public System.Drawing.CopyPixelOperation copyPixelOpType = CopyPixelOperation.SourceCopy;


        private int iterationsTemp;

        internal bool needLock;
        internal bool otherNeedsLock;

        int xCoord;
        int yCoord;

        Stopwatch fromStartStopWatch;

        private void ScreenPolling() {

            fromStartStopWatch = Stopwatch.StartNew();
            var timer = Stopwatch.StartNew();

            MakeBitmap();
                                    
            while(true) {

                if (!MainWindow.instance.inOperation) {
                    Thread.Sleep(100);
                    continue;
                }

                try {
                    TryRunTick();
                }
                catch(System.Exception e) {
                    Console.WriteLine(e);
                }
               
            
                iterationsTemp++;
                iterationsRan++;
                if(timer.ElapsedMilliseconds > 1000) {
                    iterationsLastSecond = iterationsTemp;
                    iterationsTemp = 0;
                    timer.Reset();
                    timer.Start();
                }
            }

        }

        long lastCaptureMS;
        public long lastCaptureTookMS;

        public long lastProcessingTookMS;

        int minWaitMS = 16;

        private void TryRunTick() {

            /*long needWaitMS = (lastCaptureMS + minWaitMS) - fromStartStopWatch.ElapsedMilliseconds;
            if (needWaitMS > 0) Thread.Sleep((int)needWaitMS);*/

            //var timer = System.Diagnostics.Stopwatch.StartNew();
            while (otherNeedsLock) ;
            //var waited = timer.ElapsedMilliseconds;
            //Console.WriteLine("poller waited " + waited + " ms");

            needLock = true;
            var beforeCap = fromStartStopWatch.ElapsedMilliseconds;
            graphicsThing = Graphics.FromImage(capturingIntoBitMap);
            graphicsThing.CopyFromScreen(xCoord, yCoord, 0, 0, capturingIntoBitMap.Size, copyPixelOpType);
            lastCaptureTookMS = fromStartStopWatch.ElapsedMilliseconds - beforeCap;

            lastCaptureMS = fromStartStopWatch.ElapsedMilliseconds;

            var beforeProcessing = fromStartStopWatch.ElapsedMilliseconds;
            GotNewCapture(capturingIntoBitMap);
            lastProcessingTookMS = fromStartStopWatch.ElapsedMilliseconds - beforeProcessing;

            needLock = false;

            //System.Threading.Thread.Sleep(100);
        }

        public void SetTargetArea(System.Windows.Point inPoint) {
            xCoord = (int)inPoint.X - (int)((double)width * 0.5);
            yCoord = (int)inPoint.Y - (int)((double)height * 0.5);
        }

        Bitmap previousUnchangedBitMap = null;

        public Bitmap changeBitMap = null;

        public long lastChangedPixelsCount = 0;

        public bool doDebugImage = false;

        private void GotNewCapture(Bitmap inCapturedBitMap) {
            if (previousUnchangedBitMap == null) {
                previousUnchangedBitMap = new Bitmap(inCapturedBitMap);
                if(doDebugImage)changeBitMap = new Bitmap(inCapturedBitMap);
            }

            long changedPixelsCount = 0;


            int sparseVal = pixelSkip;
                       

            for (int y = 0; y < height; y+= sparseVal) {
                for (int x = 0; x < width; x+= sparseVal) {                    

                    var prevColor = previousUnchangedBitMap.GetPixel(x, y);
                    var currColro = inCapturedBitMap.GetPixel(x, y);

                    /*var subs = GetSubstractedColor(prevColor, currColro);
                    changeBitMap.SetPixel(x,y,subs);

                    bool isChanged =
                    (System.Math.Abs(subs.R) > colorTreshold) ||
                    (System.Math.Abs(subs.G) > colorTreshold) ||
                    (System.Math.Abs(subs.B) > colorTreshold);*/

                    bool isChanged = false;

                    var changeColTemp = currColro.R - prevColor.R;
                    if(ValAbsIsOverTresh(changeColTemp,tresholdColorDifference)) isChanged = true;
                    else {
                        changeColTemp = currColro.G - prevColor.G;
                        if (ValAbsIsOverTresh(changeColTemp, tresholdColorDifference)) isChanged = true;
                        else {
                            changeColTemp = currColro.B - prevColor.B;
                            if (ValAbsIsOverTresh(changeColTemp, tresholdColorDifference)) isChanged = true;
                        }
                    }
                    

                    if (isChanged) changedPixelsCount++;                    
                }
                if (changedPixelsCount == tresholdColorDifference + 1) break; //optimization
            }
            changedPixelsCount *= sparseVal;

            lastChangedPixelsCount = changedPixelsCount;

            //var treshold = (width * height) * 0.1f;            

            if (lastChangedPixelsCount > tresholdPixelsChangedCount) {
                previousUnchangedBitMap = new Bitmap(inCapturedBitMap);
                JustGotAChangedFrame();
            }

            smoothedFPS = Lerp(smoothedFPS, instantFPS, 0.1f);
        }

        static bool ValAbsIsOverTresh(int inVal, int tresh) {
            return (inVal > tresh || inVal < -tresh);
        }

        /*private void ProcessingData() {
            while(true) {

            }
        }*/

        long lastGotFrameMS = 0;

        public float instantFPS = -1f;
        public float smoothedFPS = -1f;

        public bool hasNotShownThatFrameChanged;

        public long tresholdPixelsChangedCount = 100;
        public int tresholdColorDifference = 10;
        public int pixelSkip = 2;

        private void JustGotAChangedFrame() {
            var nowMS = fromStartStopWatch.ElapsedMilliseconds;
            var delta = nowMS - lastGotFrameMS;

            instantFPS = 1f / ((float)delta / 1000f);            

            hasNotShownThatFrameChanged = true;

            lastGotFrameMS = nowMS;
        }
        

        private float Lerp(float from, float to, float t) {
            var delt = to - from;

            var res = from + delt * t;

            return res;
        }

        static private int Clamp(int inByt, int min, int max) {
            if (inByt < min) return min;
            if (inByt > max) return max;
            return inByt;
        }

        static private int ClampToByt(int inVal) {
            var clamped = Clamp(inVal, 0, 255);
            return clamped;
        }

        static Color GetSubstractedColor(Color colA, Color colB) {
            //var col = new Color(colA.R - colB.R, colA.G - colB.G, colA.B - colB.B);
            /*var col = new Color();
            col.R = colA.R - colB.R;
            col.G = colA.G - colB.G;
            col.B = colA.B - colB.B;*/

            var col = Color.FromArgb(255, ClampToByt(colA.R - colB.R), ClampToByt(colA.G - colB.G), ClampToByt(colA.B - colB.B));

            return col;
        }

        private void MakeBitmap() {
            rekt = new Rectangle(xCoord, yCoord, width, height);
            capturingIntoBitMap = new Bitmap(rekt.Width, rekt.Height, PixelFormat.Format32bppArgb);
        }
    }
}