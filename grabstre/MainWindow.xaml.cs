﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace grabstre
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static MainWindow instance;
        private ScreenPoller screenPoller;

        public bool inOperation = true;

        public MainWindow() {
            InitializeComponent();
            instance = this;

            //maininfo.Content = "starting";

            var enumvals = Enum.GetNames(typeof(CopyPixelOperation));
            copyPixOpSelector.ItemsSource = enumvals;
            copyPixOpSelector.SelectionChanged += CopyPixOpSelector_SelectionChanged;


            sliderHoriz.Maximum = 1;
            sliderVerti.Maximum = 1;
            
            screenPoller = new ScreenPoller();
            screenPoller.Start();

            pixCountTreshBox.Text = screenPoller.tresholdPixelsChangedCount.ToString();
            colorChangeTreshBox.Text = screenPoller.tresholdColorDifference.ToString();

            System.Threading.Thread.Sleep(100);

            screenRefreshTimer = System.Diagnostics.Stopwatch.StartNew();

            var t = new Thread(ForceUpdatingLoop);
            t.Name = "ForceUpdatingLoop";
            t.IsBackground = true;
            t.Start();
        }

        int errCount = 0;

        private void ForceUpdatingLoop() {
            while(true) {

                if(!inOperation) {
                    Thread.Sleep(100);
                    continue;
                }

                try { 
                    App.Current.Dispatcher.Invoke(() => {
                        Redraw();
                        HandleKeyboardInput();
                    });
                }
                catch(System.Exception e) {                    
                    Console.WriteLine(e);
                    screenPoller.otherNeedsLock = false; //prevent softlock

                    errCount++;
                    if (errCount > 100) throw e;
                }
                waitingForReDraw = true;

                //while (waitingForReDraw) {
                    Thread.Sleep(16);
                //}
            }
        }

        private void HandleKeyboardInput() {
            var isHolding = Keyboard.IsKeyDown(Key.LeftCtrl);            

            if(isHolding) {
                var mousePos = GetMousePosition();
                screenPoller.SetTargetArea(mousePos);
            }
        }

        public System.Windows.Point GetMousePosition() {
            System.Drawing.Point point = System.Windows.Forms.Control.MousePosition;
            return new System.Windows.Point(point.X, point.Y);
        }

        /*System.Windows.Point GetMousePos() {
            return this.PointToScreen(Mouse.GetPosition(this));
        }*/

        //int bmpCopyLastTookMS = -1;

        bool waitingForReDraw = false;

        private void Redraw() {

            MainWindow.instance.aliveMarker.Content = "its:" + screenPoller.iterationsRan;
            MainWindow.instance.fpsmeter.Content = "its/s:" + screenPoller.iterationsLastSecond;
            MainWindow.instance.fpsMeterScreen.Content = "screen:" + screenRefreshesLastSec;

            capTookMSText.Content = screenPoller.lastCaptureTookMS;
            processingTookMS.Content = screenPoller.lastProcessingTookMS;
            //Console.WriteLine("shouldbe " + screenPoller.iterationsLastSecond);

            contentFpsMeterInstant.Content = Math.Round(screenPoller.instantFPS,2).ToString();
            contentFpsMeterSmooth.Content = Math.Round(screenPoller.smoothedFPS,0).ToString();

            fpsVizSlider.Value = screenPoller.smoothedFPS;

            var showChanged = screenPoller.hasNotShownThatFrameChanged;
            screenPoller.hasNotShownThatFrameChanged = false;
            frameChangedOverlay.Visibility = showChanged ? Visibility.Visible : Visibility.Hidden;

            var showLowRateWarning = screenPoller.iterationsLastSecond < 50;
            capturerateWarning.Visibility = showLowRateWarning ? Visibility.Visible : Visibility.Hidden;

            lastPixelsChangedCountViewer.Content = screenPoller.lastChangedPixelsCount.ToString();
            

            DoImageCopying();            
            

        }

        private void DoImageCopying() {

            while (screenPoller.needLock) ;
            screenPoller.otherNeedsLock = true;


            //screenPoller.xCoord = (int)(sliderHoriz.Value * (double)System.Windows.SystemParameters.PrimaryScreenWidth);
            //screenPoller.yCoord = ((int)System.Windows.SystemParameters.PrimaryScreenHeight) - (int)(sliderVerti.Value * (double)System.Windows.SystemParameters.PrimaryScreenHeight);
            //Console.WriteLine("refress");

            var betr = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                screenPoller.capturingIntoBitMap.GetHbitmap(),
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());

            image.Source = betr;

            if (screenPoller.changeBitMap != null) {
                var deltaBetr = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                    screenPoller.changeBitMap.GetHbitmap(),
                    IntPtr.Zero,
                    Int32Rect.Empty,
                    BitmapSizeOptions.FromEmptyOptions());

                deltaImage.Source = deltaBetr;
            }


            screenPoller.otherNeedsLock = false;
            waitingForReDraw = false;


            screenRefreshesTemp++;
            if (screenRefreshTimer.ElapsedMilliseconds > 1000) {
                screenRefreshesLastSec = screenRefreshesTemp;
                screenRefreshesTemp = 0;
                screenRefreshTimer.Reset();
                screenRefreshTimer.Start();
            }

            //image.Source = new BitmapImage(new Uri(@"C:\testImg.jpg"));
        }

        int screenRefreshesTemp;
        int screenRefreshesLastSec;
        private Stopwatch screenRefreshTimer;

        private void CopyPixOpSelector_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            Console.WriteLine(e);

            //var selected = copyPixOpSelector.SelectionBoxItem;
            var selected = e.AddedItems[0];
            Console.WriteLine(selected);

            var vals = (int[])Enum.GetValues(typeof(CopyPixelOperation));
            var nameindex = Array.IndexOf(Enum.GetNames(typeof(CopyPixelOperation)), selected);
            var asenum = (CopyPixelOperation)(vals[nameindex]);

            Console.WriteLine(asenum);

            screenPoller.copyPixelOpType = asenum;

            //screenPoller.copyPixelOpType = 
            //screenPoller.copyPixelOpType = comb
        }

        private void OnPressedStartStop(object sender, RoutedEventArgs e) {
            inOperation = !inOperation;

            Console.WriteLine("bress " + inOperation);

            StartStopButton.Content = inOperation ? "Stop" : "Start";
        }

        private void DidChangePixCountTresh(object sender, TextChangedEventArgs e) {

            if (screenPoller == null) return;

            //Console.WriteLine(pixCountTreshBox.Text);
            int parsed;
            if(int.TryParse(pixCountTreshBox.Text, out parsed)) {                 
                screenPoller.tresholdPixelsChangedCount = parsed;
            }
        }

        private void DidChangeColorTresh(object sender, TextChangedEventArgs e) {

            if (screenPoller == null) return;

            int parsed;
            if (int.TryParse(colorChangeTreshBox.Text, out parsed)) {
                screenPoller.tresholdColorDifference = parsed;
            }
        }

        private void DidChangePixelSkip(object sender, TextChangedEventArgs e) {
            if (screenPoller == null) return;

            int parsed;
            if (int.TryParse(pixelSkipBox.Text, out parsed)) {
                screenPoller.pixelSkip = parsed;
            }           

        }

        private void ShowHelp(string v) {
            MessageBox.Show(v);
        }

        private void OnPressedPixelChangeTreshHelp(object sender, RoutedEventArgs e) {
            ShowHelp("Denotes the amount of pixels that need to change between frames for a change to register.");
        }        

        private void OnPressedColorTreshHelp(object sender, RoutedEventArgs e) {
            ShowHelp("Denotes how much a color needs to change to be considered changed. Value is checked for all color channels (red, green, blue) and the values used are bytes (0 for no red, 255 for as red as possible)");
        }

        private void OnPressedPixelSkipHelp(object sender, RoutedEventArgs e) {
            ShowHelp("Pixel skip: every nth pixel is taken in to calculation. Default is 1 but lower this value if you have performance problems.");
        }
    }
}
